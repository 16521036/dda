#include "Parapol.h"

void Draw2Points1(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    SDL_RenderDrawPoint(ren, xc + x, yc + y);
    SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void Draw2Points2(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x0 = 0;
    int y0= 0;
    int p0 = 1 - A;
    while (x0 < A)
    {
        Draw2Points1(xc,yc,x0,y0,ren);
        if (p0<= 0) p0 = p0 + 2*x0 + 3;
        else
        {
            y0++;
            p0 = p0 + 2*x0 + 3 - 2*A;
        }
        x0++;
    }

    int x1 = A;
    int y1 = A/2;

    int p1 = 2*A - 1;

    while(y1 < 4000)
    {
        Draw2Points1(xc,yc,x1,y1,ren);
        if (p1 <= 0) p1 = p1 + 4*A;
        else
        {
            x1++;
            p1 = p1 + 4*A - 4*x1 - 4;
        }
        y1 ++;
    }
}
void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x0 = 0;
	int y0 = 0;
	int p0 = 1 - A;
	while (x0 < A)
	{
		Draw2Points2(xc, yc, x0, y0, ren);
		if (p0 <= 0) p0 = p0 + 2 * x0 + 3;
		else
		{
			y0++;
			p0 = p0 + 2 * x0 + 3 - 2 * A;
		}
		x0++;
	}

	int x1 = A;
	int y1 = A / 2;

	int p1 = 2 * A - 1;

	while (y1 < 4000)
	{
		Draw2Points2(xc, yc, x1, y1, ren);
		if (p1 <= 0) p1 = p1 + 4 * A;
		else
		{
			x1++;
			p1 = p1 + 4 * A - 4 * x1 - 4;
		}
		y1++;
	}
}