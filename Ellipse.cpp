#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
    SDL_RenderDrawPoint(ren, xc - x, yc + y);
    SDL_RenderDrawPoint(ren, xc + x, yc - y);
    SDL_RenderDrawPoint(ren, xc - x, yc - y);
}
void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	
		int x, y; float c, p;
		x = 0; y = b;
		c = (float)b / a;
		c = c * c; p = 2 * c - 2 * b + 1;
		while (c*x <= y)
		{
			Draw4Points(xc, yc, x, y, ren);
			if (p < 0) p += 2 * c*(2 * x + 3);
			else
			{
				p += 4 * (1 - y) + 2 * c*(2 * x + 3);
				y--;
			}
			x++;
		}

		y = 0; x = a;
		c = (float)a / b;
		c = c * c; p = 2 * c - 2 * x + 1;
		while (c*y <= x)
		{
			Draw4Points(xc, yc, x, y, ren);
			if (p < 0) p += 2 * c*(2 * y + 3);
			else
			{
				p += 4 * (1 - x) + 2 * c*(2 * y + 3);
				x--;
			}
			y++;
		}
	
}
void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	int x0 = 0; 
	int y0= b;
	int p0 = b * b - a*a*b + a * a/4;
	Draw4Points(xc, yc, x0, y0, ren);
	while (x0*x0*(a*a + b * b) <= a*a*a*a)
	{
		if (p0 <= 0)
			p0 += 2*b*b*x0 + 3*b*b;

		else
		{
			p0 += 2*b*b*x0 - 2*a*a*y0 + 2*a*a + 3*b*b;
			y0 = y0 - 1;
		}
		x0 = x0 + 1;
		Draw4Points(xc, yc, x0, y0, ren);
	}
    // Area 2
	int x1 = a;
	int y1 = 0;
	int p1 = a * a - b * b*a + b * b / 4;
	while (x1*x1*(a*a + b * b) >= a*a*a*a)
	{
		if (p1 <= 0)
			p1 += 2 * a*a*y1 + 3 * a*a;

		else
		{
			p1 += 2 * a*a*y1 - 2 * b*b*x1 + 2 * b*b + 3 * a*a;
			x1 = x1 - 1;
		}
		y1 = y1 + 1;
		Draw4Points(xc, yc, x1, y1, ren);
	}
}