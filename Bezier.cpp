#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	float x, y;
	for (float t = 0.0; t <= 1.0; t += 0.001)
	{
		float t1 = (1 - t)*(1 - t);
		float t2 = 2 * (1 - t)*t;
		float t3 = t * t;
		x = t1 * p1.x + t2 * p2.x + t3 * p3.x;
		y = t1 * p1.y + t2 * p2.y + t3 * p3.y;
		SDL_RenderDrawPoint(ren, int(x + 0.5), int(y + 0.5));
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	float x, y;
	for (float t = 0.0; t <= 1.0; t += 0.001)
	{
		float t1 = (1 - t)*(1 - t)*(1 - t);
		float t2 = 3 * (1 - t)*(1 - t)*t;
		float t3 = 3 * (1 - t)*t*t;
		float t4 = t * t*t;
		x = t1 * p1.x + t2 * p2.x + t3 * p3.x + t4 * p4.x;
		y = t1 * p1.y + t2 * p2.y + t3 * p3.y + t4 * p4.y;
		SDL_RenderDrawPoint(ren, int(x + 0.5), int(y + 0.5));
	}
}


